<?php

namespace Database\Factories;

use App\Models\Room;
use App\Models\Schedule;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

use function Psy\debug;

class ScheduleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Schedule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $statuses = collect([
            'requested',
            'approved',
            'rejected',
        ]);

        $status = $statuses->random();

        $approved_by = null;
        if ($status == 'approved') {
            $approved_by = User::all()->random()->id;
        }

        $date = $this->faker->dateTimeBetween('-1 month');

        //lamanya peminjaman
        $hour = $this->faker->numberBetween(1, 4);

        //jam mulai peminjaman
        $start_hour = $this->faker->numberBetween(7, 17);
        $end_hour = $start_hour + $hour;

        $time_range = [];
        for ($i = $start_hour; $i <= $end_hour; $i++) {
            array_push($time_range, $i);
        }

        return [
            'user_id' => User::all()->random()->id,
            'room_id' => Room::all()->random()->id,
            'date'  => $date,
            'time_start'  => $start_hour,
            'time_end'  => $end_hour,
            'time_range' => json_encode($time_range),
            'status' => 'approved',
            'approved_by' => '1',
            'remarks' => $this->faker->paragraph()
        ];
    }
}
