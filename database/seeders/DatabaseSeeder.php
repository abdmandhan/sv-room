<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'KOMUNIKASI', 'username' => 'kmn'],
            ['name' => 'EKOWISATA', 'username' => 'ekw'],
            ['name' => 'MANAJEMEN INFORMATIKA', 'username' => 'inf'],
            ['name' => 'TEKNIK KOMPUTER', 'username' => 'tek'],
            ['name' => 'SUPERVISOR JAMINAN MUTU PANGAN', 'username' => 'jmp'],
            ['name' => 'MANAJEMEN INDUSTRI JASA MAKANAN DAN GIZI', 'username' => 'gzi'],
            ['name' => 'TEKNOLOGI INDUSTRI BENIH', 'username' => 'tib'],
            ['name' => 'TEKNOLOGI PRODUKSI DAN MANAJEMEN PERIKANAN BUDIDAYA', 'username' => 'ikn'],
            ['name' => 'TEKNOLOGI DAN MANAJEMEN TERNAK', 'username' => 'tnk'],
            ['name' => 'MANAJEMEN AGRIBISNIS', 'username' => 'mab'],
            ['name' => 'MANAJEMEN INDUSTRI', 'username' => 'mni'],
            ['name' => 'ANALISIS KIMIA', 'username' => 'kim'],
            ['name' => 'TEKNIK DAN MANAJEMEN LINGKUNGAN', 'username' => 'lnk'],
            ['name' => 'AKUNTANSI', 'username' => 'akn'],
            ['name' => 'PARAMEDIK VETERINER', 'username' => 'pvt'],
            ['name' => 'TEKNOLOGI PRODUKSI DAN MANAJEMEN PERKEBUNAN', 'username' => 'tmp'],
            ['name' => 'TEKNOLOGI PRODUKSI DAN PENGEMBANGAN MASYARAKAT PERTANIAN', 'username' => 'ppp'],
            ['name' => 'Petugas CA', 'username' => 'ca'],
            ['name' => 'Petugas CB', 'username' => 'cb'],
            ['name' => 'Petugas GG', 'username' => 'gg'],
            ['name' => 'Petugas BS', 'username' => 'bs'],
        ];

        User::create([
            'username' => 'akademik',
            'name' => 'AKADEMIK',
            'password' => Hash::make('12341234'),
            'is_admin' => true,
        ]);

        foreach ($users as $key => $value) {
            User::create([
                'username' => $value['username'],
                'name' => $value['name'],
                'password' => Hash::make($value['username'] . '1234'),
            ]);
        }


        $this->call([
            BuildingSeeder::class,
            ScheduleSeeder::class,
            UserColor::class,
        ]);
    }
}
