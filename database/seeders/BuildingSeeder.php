<?php

namespace Database\Seeders;

use App\Models\Building;
use App\Models\Room;
use Illuminate\Database\Seeder;

class BuildingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $buildings = [
            [
                'name' => 'CA',
                'rooms' => [
                    ['name' => 'CA B01'],
                    ['name' => 'CA B02'],
                    ['name' => 'CA B03'],
                    ['name' => 'CA B04'],
                    ['name' => 'CA B05'],
                    ['name' => 'CA B06'],
                ]
            ],
            [
                'name' => 'CB',
                'rooms' => [
                    ['name' => 'CB B01'],
                    ['name' => 'CB B02'],
                    ['name' => 'CB B03'],
                    ['name' => 'CB B04'],
                    ['name' => 'CB B05'],
                    ['name' => 'CB B06'],
                ]
            ],
            [
                'name' => 'GG',
                'rooms' => [
                    ['name' => 'GG B10'],
                ]
            ],
            [
                'name' => 'BS',
                'rooms' => [
                    ['name' => 'BS B01'],
                    ['name' => 'BS B02'],
                    ['name' => 'BS B03'],
                    ['name' => 'BS B04'],
                    ['name' => 'BS B05'],
                    ['name' => 'BS B06'],
                    ['name' => 'BS B07'],
                    ['name' => 'BS B08'],
                    ['name' => 'BS B09'],
                    ['name' => 'BS B10'],
                    ['name' => 'BS KIMIA'],
                    ['name' => 'BS FISIKA'],
                    ['name' => 'BS BOTANI'],
                    ['name' => 'BS P01'],
                    ['name' => 'BS P02'],
                    ['name' => 'BS P03'],
                ]
            ],
        ];

        foreach ($buildings as $key => $value) {
            $building = Building::create(['name' => $value['name']]);

            foreach ($value['rooms'] as $a => $b) {
                Room::create([
                    'building_id' => $building['id'],
                    'name' => $b['name'],
                    'capacity' => 0,
                ]);
            }
        }
    }
}
