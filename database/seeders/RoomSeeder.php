<?php

namespace Database\Seeders;

use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $rooms = [
            [
                // 'room_type_id' => 1,
                'name' => 'CA 1-1',
                'capacity' => 70,
                'building_id' => 1,
            ],
            [
                // 'room_type_id' => 1,
                'name' => 'CA 1-2',
                'capacity' => 70,
                'building_id' => 1,
            ],
            [
                // 'room_type_id' => 1,
                'name' => 'CA 1-3',
                'capacity' => 70,
                'building_id' => 1,
            ],
            [
                // 'room_type_id' => 1,
                'name' => 'CB 1-3',
                'capacity' => 70,
                'building_id' => 2,

            ],
            [
                // 'room_type_id' => 1,
                'name' => 'CB 2-3',
                'capacity' => 70,
                'building_id' => 2,

            ],
            [
                // 'room_type_id' => 1,
                'name' => 'CB 3-3',
                'capacity' => 70,
                'building_id' => 2,

            ],
            [
                // 'room_type_id' => 1,
                'name' => 'GG K01',
                'capacity' => 70,
                'building_id' => 3,

            ],
            [
                // 'room_type_id' => 1,
                'name' => 'GG K02',
                'capacity' => 70,
                'building_id' => 3,

            ],
            [
                // 'room_type_id' => 1,
                'name' => 'GG K03',
                'capacity' => 70,
                'building_id' => 3,

            ],
        ];

        foreach ($rooms as $key => $value) {
            Room::create($value);
        }
    }
}


// [
    // 'room_type_id' => 1,
//     'name' => 'CA 2-1',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 2-2',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 2-3',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 3-1',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 3-2',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 3-3',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 4-1',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 4-2',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
// [
    // 'room_type_id' => 1,
//     'name' => 'CA 4-3',
//     'capacity' => 70,
//     'building_id' => 1,
// ],
