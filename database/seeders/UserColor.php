<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserColor extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = [
            [
                'name' => 'KOMUNIKASI',
                'username' => 'kmn',
                'color' => '#000000',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'EKOWISATA',
                'username' => 'ekw',
                'color' => '#039581',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'MANAJEMEN INFORMATIKA',
                'username' => 'inf',
                'color' => '#322264',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'TEKNIK KOMPUTER',
                'username' => 'tek',
                'color' => '#a9adaf',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'SUPERVISOR JAMINAN MUTU PANGAN',
                'username' => 'jmp',
                'color' => '#0151a1',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'MANAJEMEN INDUSTRI JASA MAKANAN DAN GIZI',
                'username' => 'gzi',
                'color' => '#f66323',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'TEKNOLOGI INDUSTRI BENIH',
                'username' => 'tib',
                'color' => '#495532',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'TEKNOLOGI PRODUKSI DAN MANAJEMEN PERIKANAN BUDIDAYA',
                'username' => 'ikn',
                'color' => '#212e3b',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'TEKNOLOGI DAN MANAJEMEN TERNAK',
                'username' => 'tnk',
                'color' => '#a4acb9',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'MANAJEMEN AGRIBISNIS',
                'username' => 'mab',
                'color' => '#21313d',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'MANAJEMEN INDUSTRI',
                'username' => 'mni',
                'color' => '#b8cad6',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'ANALISIS KIMIA',
                'username' => 'kim',
                'color' => '#62121e',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'TEKNIK DAN MANAJEMEN LINGKUNGAN',
                'username' => 'lnk',
                'color' => '#cbbead',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'AKUNTANSI',
                'username' => 'akn',
                'color' => '#d1d5d8',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'PARAMEDIK VETERINER',
                'username' => 'pvt',
                'color' => '#a262aa',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'TEKNOLOGI PRODUKSI DAN MANAJEMEN PERKEBUNAN',
                'username' => 'tmp',
                'color' => '#986851',
                'text_color' => '#FFFFFF'
            ],
            [
                'name' => 'TEKNOLOGI PRODUKSI DAN PENGEMBANGAN MASYARAKAT PERTANIAN',
                'username' => 'ppp',
                'color' => '#4d4240',
                'text_color' => '#FFFFFF'
            ],
        ];

        foreach ($users as $key => $value) {
            User::where('username', $value['username'])->update([
                'color' => $value['color'],
                'text_color' => $value['text_color']
            ]);
        }
    }
}
