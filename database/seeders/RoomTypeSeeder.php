<?php

namespace Database\Seeders;

use App\Models\RoomType;
use Illuminate\Database\Seeder;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $room_type = [
            ['name' => 'Kuliah'],
            ['name' => 'Laboratorium Komputer'],
            ['name' => 'Laboratorium Gizi'],
            ['name' => 'Laboratorium Kimia'],
        ];

        foreach ($room_type as $key => $value) {
            RoomType::create($value);
        }
    }
}
