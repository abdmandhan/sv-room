<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BuildingController;
use App\Http\Controllers\Api\RoomController;
use App\Http\Controllers\Api\ScheduleController;
use App\Http\Controllers\Api\UserController;
use App\Models\Building;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('verify', [AuthController::class, 'verify']);
    Route::post('logout', [AuthController::class, 'logout']);

    Route::get('schedule', [ScheduleController::class, 'index']);
    Route::get('schedule/{id}', [ScheduleController::class, 'show']);
    Route::get('requested-schedule', [ScheduleController::class, 'requestedSchedule']);
    Route::post('schedule', [ScheduleController::class, 'store']);
    Route::post('schedule-room', [ScheduleController::class, 'store_room']);
    Route::post('schedule-check-import', [ScheduleController::class, 'checkImport']);
    Route::post('schedule/{id}/change-status/{status}', [ScheduleController::class, 'changeStatus']);
    Route::delete('schedule/{id}', [ScheduleController::class, 'destroy']);

    Route::resource('room', RoomController::class);
    Route::resource('building', BuildingController::class);
    Route::resource('user', UserController::class);

    Route::post('upload', function (Request $request) {
        $request->validate([
            'image' => ['required', 'file', 'mimes:jpg,jpeg,png']
        ]);

        $filename = now()->timestamp . '.' . $request->file('image')->extension();

        $request->file('image')->storeAs("/zbuildings/", $filename);

        return response()->json([
            'url' => "https://roomsvipb.my.id/zbuildings/$filename"
            // 'url' => "http://localhost:8010/buildings/$filename"
        ]);
    });

    Route::get('options', function () {
        return response()->json([
            'buildings' => Building::all(['id', 'name', 'image']),
            'rooms'     => Room::with(['building:id,name'])->get(['id', 'name', 'building_id'])
        ]);
    });
});
