// mutation types
const SET_ERROR = "m_error";
const SET_LOADING = "m_loading";
const SET_DRAWER = "m_drawer";
const SET_DIALOG = "m_dialog";
const SET_DIALOG_ROOM = "m_dialog_room";

export const state = () => ({
  errors: {},
  loading: false,
  drawer: true,
  dialog: false,
  dialog_room: false,
});

export const actions = {
  startLoading(context) {
    context.commit(SET_LOADING, true);
  },
  finishLoading(context) {
    context.commit(SET_LOADING, false);
  },
  toogleDrawer(context) {
    context.commit(SET_DRAWER, !context.state.drawer);
  },
  openDrawer(context) {
    context.commit(SET_DRAWER, true);
  },
  closeDrawer(context) {
    context.commit(SET_DRAWER, false);
  },
  openDialog(context) {
    context.commit(SET_DIALOG, true);
  },
  closeDialog(context) {
    context.commit(SET_DIALOG, false);
  },
  openDialogRoom(context) {
    context.commit(SET_DIALOG_ROOM, true);
  },
  closeDialogRoom(context) {
    context.commit(SET_DIALOG_ROOM, false);
  },
};

export const mutations = {
  [SET_ERROR](state, data) {
    state.errors = data;
    // console.log('setError', state.errors);
  },
  [SET_LOADING](state, data) {
    state.loading = data;
    // console.log('setLoading', state.loading);
  },
  [SET_DRAWER](state, data) {
    state.drawer = data;
    // console.log('setDrawer', state.drawer);
  },
  [SET_DIALOG](state, data) {
    state.dialog = data;
    // console.log('setDialog', state.dialog);
  },
  [SET_DIALOG_ROOM](state, data) {
    state.dialog_room = data;
    // console.log('setDialog', state.dialog);
  },
};
