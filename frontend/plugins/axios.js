export default function ({ $axios, redirect, $toast }) {
  $axios.onRequest(config => {
    // console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {

    const code = parseInt(error.response && error.response.status)
    // console.log('code', code);
    $toast.error(error.response.data.message);

    // switch (code) {
    //   case 429:
    //     $toast.error(error.response.data.message);
    //     break;

    //   default:
    //     $toast.error(error.response.data.message);
    //     break;
    // }

    // if (code === 400) {
    //   redirect('/400')
    // }
  })
}
