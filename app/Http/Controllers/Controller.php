<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($data, $message = 'success')
    {
        return response()->json([
            'data' => $data,
            'message' => $message
        ]);
    }

    public function failed($message = 'failed', $data = null)
    {
        return response()->json([
            'data' => $data,
            'message' => $message
        ], 400);
    }
}
