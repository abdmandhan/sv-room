<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function login(Request $request)
    {
        $data = (object) $request->validate([
            'username'  => ['required', 'exists:users'],
            'password'  => ['required']
        ]);

        $user = User::where('username', $data->username)->first();

        if (Hash::check($data->password, $user->password)) {
            // $user->tokens()->delete();
            return $this->success(
                [
                    'token' => $user->createToken('auth')->plainTextToken,
                    'user' => $user,
                ],
                'berhasil login'
            );
        } else {
            return $this->failed('username atau password salah');
        }
    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'username'  => ['required', 'unique:users'],
            'password'  => ['required', 'confirmed']
        ]);

        $user = User::create(array_merge($data, ['name' => $data['username']]));

        return $this->success(
            [
                'token' => $user->createToken('auth')->plainTextToken,
                'user' => $user,
            ],
            'akun berhasil didaftarkan'
        );
    }

    public function verify(Request $request)
    {
        return $this->success([
            'token' => $request->user()->createToken('auth')->plainTextToken,
            'user' => $request->user(),
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return $this->success(null, 'success logout');
    }
}
