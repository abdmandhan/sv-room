<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Building;
use App\Models\Room;
use App\Models\Schedule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    //
    public function index()
    {
        return $this->success(Room::with([
            'room_type', 'building'
        ])->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'          => ['required'],
            'capacity'      => ['required', 'numeric'],
            'building_id'   => ['required', 'exists:buildings,id'],
            'is_active'       => ['required']
        ]);

        Room::create($data);

        return $this->success(null, 'berhasil membuat room');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->success([
            'room' => Room::with(['schedule.user'])->findOrFail($id),
            'building' => Building::all(['id', 'name'])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name'          => ['required'],
            'capacity'      => ['required', 'numeric'],
            'building_id'   => ['required', 'exists:buildings,id'],
            'is_active'       => ['required']
        ]);

        Room::find($id)->update($data);

        return $this->success(null, 'berhasil update room');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Schedule::where('room_id', $id)->delete();
        // if ($schedule) return $this->failed('tidak bisa menghapus ruangan, ruangan sudah mempunyai jadwal');

        Room::find($id)->delete();

        return $this->success(null, 'berhasil menghapus ruangan');
    }
}
