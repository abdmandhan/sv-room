<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\Schedule;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ScheduleController extends Controller
{
    //
    public function index(Request $request)
    {
        return $this->success(Room::with([
            'room_type',
            'building',
            'schedule.user:id,name,username,color,text_color',
            'schedule' => function ($q) use ($request) {
                if ($request->status) {
                    $q->where('status', $request->status);
                }
                if ($request->date) {
                    $q->where('date', $request->date);
                }
            }
        ])
            ->whereHas('building', function (Builder $q) use ($request) {
                if ($request->building) {
                    $q->where('name', $request->building);
                }
            })
            ->where('is_active', true)
            ->get());
    }

    public function requestedSchedule(Request $request)
    {
        $data = Schedule::select(['id', 'user_id', 'room_id', 'date', 'status', 'time_start', 'time_end', 'created_at']);

        if ($request->date) $data = $data->where('date', $request->date);

        $data = $data->where('status', '!=', 'approved')
            ->orderBy('created_at', 'desc')
            ->with(['user:id,name', 'room:id,name,building_id', 'room.building'])->get();

        return $this->success($data);
    }

    public function show(Request $request)
    {
        return $this->success(Schedule::with(['user:id,name', 'room:id,name,building_id', 'room.building'])->find($request->id));
    }

    public function store(Request $request)
    {
        // DB::beginTransaction();
        $data = $request->validate([
            'room_id'       => ['required', 'exists:rooms,id'],
            'date'          => ['required', 'date'],
            'time_start'    => ['required', 'numeric', 'min:6', 'max:23'],
            'time_end'      => ['required', 'numeric', 'min:6', 'max:23'],
            'remarks'       => ['nullable'],
            'telp'          => ['required'],
            'name'          => ['required'],
            'dates'         => ['array']
        ]);

        //validate kalau selain admin gaboleh kirim dates array
        if (isset($data['dates'])) {
            if (!Auth::user()->is_admin) {
                return $this->failed('unauthorized', count($data['dates']));
            }
        } else $data['dates'] = [];

        //validate date
        $current_week = (new DateTime())->format("W");
        $request_week = (new DateTime($data['date']))->format("W");

        //new year condition
        // if (!($current_week >= 52 && $request_week == 1)) {
        //     if ($current_week != $request_week) {
        //         if ($current_week + 1 != $request_week) return $this->failed('jadwal hanya bisa dibuat di minggu ini atau minggu depan');
        //     }
        // }

        if ($data['time_start'] > $data['time_end']) return $this->failed('jam mulai tidak boleh lebih kecil dari jam selesai');

        $validate_schedule = Schedule::validate_schedule(
            $data['room_id'],
            $data['date'],
            $data['time_start'],
            $data['time_end']
        );
        if (!$validate_schedule) return $this->failed('ruangan pada tanggal ' . $data['date'] . ' sudah ada jadwal');

        //validate date array
        foreach ($data['dates'] as $key => $value) {
            $validate_schedule = Schedule::validate_schedule(
                $data['room_id'],
                $value,
                $data['time_start'],
                $data['time_end']
            );
            if (!$validate_schedule) return $this->failed('ruangan pada tanggal ' . $value . ' sudah ada jadwal');
        }

        $time_range = [];
        for ($i = $data['time_start']; $i <= $data['time_end']; $i++) {
            array_push($time_range, (int)$i);
        }

        $schedule = Schedule::create(array_merge($data, [
            'status'        => 'approved',
            'user_id'       => $request->user()->id,
            'time_range'    => json_encode($time_range),
        ]));

        //insert date array
        foreach ($data['dates'] as $key => $value) {
            Schedule::create(array_merge($data, [
                'date'          => $value,
                'status'        => 'approved',
                'user_id'       => $request->user()->id,
                'time_range'    => json_encode($time_range),
            ]));
        }

        return $this->success($schedule, 'berhasil membuat jadwal ruangan');
    }

    public function store_room(Request $request)
    {
        // DB::beginTransaction();
        $data = $request->validate([
            'date'          => ['required', 'array'],
            'date.*'        => ['required', 'date'],
            'time_start'    => ['required', 'numeric', 'min:6', 'max:23'],
            'time_end'      => ['required', 'numeric', 'min:6', 'max:23'],
            'remarks'       => ['nullable'],
            'telp'          => ['required'],
            'name'          => ['required'],
            'rooms'         => ['array', 'required']
        ]);

        //validate kalau selain admin
        if (!Auth::user()->is_admin) {
            return $this->failed('unauthorized');
        }

        if ($data['time_start'] > $data['time_end']) return $this->failed('jam mulai tidak boleh lebih kecil dari jam selesai');

        //validate room array
        foreach ($data['date'] as $a => $b) {
            foreach ($data['rooms'] as $key => $value) {
                $validate_schedule = Schedule::validate_schedule(
                    $value,
                    $b,
                    $data['time_start'],
                    $data['time_end']
                );
                if (!$validate_schedule) {
                    $room = Room::find($value);
                    return $this->failed('ruangan ' . $room->name . ' pada tanggal ' . $b . ' sudah ada jadwal');
                }
            }
        }

        $time_range = [];
        for ($i = $data['time_start']; $i <= $data['time_end']; $i++) {
            array_push($time_range, (int)$i);
        }

        //insert date array
        foreach ($data['date'] as $a => $b) {
            foreach ($data['rooms'] as $key => $value) {
                Schedule::create(array_merge($data, [
                    'room_id'       => $value,
                    'date'          => $b,
                    'status'        => 'approved',
                    'user_id'       => $request->user()->id,
                    'time_range'    => json_encode($time_range),
                ]));
            }
        }

        return $this->success(null, 'berhasil membuat jadwal ruangan');
    }

    public function changeStatus($id, $status)
    {
        $schedule = Schedule::find($id);

        if ($status == 'approved') {
            $exists = DB::table('schedules')->where('room_id', $schedule['room_id'])
                ->where('date', $schedule['date'])
                ->where('status', 'approved')
                ->whereBetween('time_start', [$schedule['time_start'], $schedule['time_end']])
                ->exists();

            if ($exists) return $this->failed('ruangan ini sudah ada jadwal');


            $exists = DB::table('schedules')->where('room_id', $schedule['room_id'])
                ->where('date', $schedule['date'])
                ->where('status', 'approved')
                ->whereBetween('time_end', [$schedule['time_start'], $schedule['time_end']])
                ->exists();

            if ($exists) return $this->failed('ruangan ini sudah ada jadwal');
        }

        $schedule->update([
            'status' => $status,
            'approved_by' => Auth::user()->id,
        ]);

        return $this->success(null, 'berhasil approve jadwal');
    }

    public function destroy($id)
    {
        //check authorization
        $schedule = Schedule::find($id);

        if ($schedule->user_id != Auth::user()->id && !Auth::user()->is_admin) return $this->failed('unauthorized');

        $schedule->delete();

        return $this->success(null, 'berhasil menghapus jadwal');
    }

    public function checkImport(Request $request)
    {
        $data = $request->validate([
            'data' => ['required', 'array']
        ]);

        $result = [];

        foreach ($data['data'] as $key => $value) {
            $room = Room::where('name', $value['room'])->first();
            if (!$room) {
                array_push($result, array_merge($value, [
                    'status' => 'failed',
                    'message' => 'room not found'
                ]));
                continue;
            }

            if ($value['start'] > $value['end']) {
                array_push($result, array_merge(
                    $value,
                    [
                        'status' => 'failed',
                        'message' => 'start time cannot be greater than end time'
                    ]
                ));
                continue;
            }

            $validate_schedule = Schedule::validate_schedule(
                $room->id,
                $value['date'],
                $value['start'],
                $value['end']
            );

            if (!$validate_schedule) {
                array_push($result, array_merge($value, [
                    'status' => 'failed',
                    'message' => 'schedule already exists'
                ]));
            } else {
                array_push($result, array_merge($value, [
                    'status' => 'success',
                    'message' => 'schedule can be created',
                    'room_id' => $room->id
                ]));
            }
        }

        return $this->success($result);
    }
}
