<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Schedule extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'room_id',
        'date',
        'time_start',
        'time_end',
        'time_range',
        'status',
        'approved_by',
        'remarks',
        'name',
        'telp'
    ];

    protected $casts = [
        'time_range' => 'array',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $appends = [
        'total_time'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function approved_by_user()
    {
        return $this->belongsTo(User::class, 'id', 'approved_by');
    }

    public function getTotalTimeAttribute()
    {
        return $this->time_end - $this->time_start;
    }

    public static function validate_schedule($room_id, $date, $time_start, $time_end)
    {
        //check room date start_hour
        $exists_start = Schedule::select(['time_start'])
            ->where('room_id', $room_id)
            ->where('date', $date)
            ->where('time_start', $time_start)
            ->where('status', 'approved')
            ->first();

        if ($exists_start) return false;

        $exists_start2 = Schedule::select(['time_end'])
            ->where('room_id', $room_id)
            ->where('date', $date)
            ->where('time_end', $time_start)
            ->where('status', 'approved')
            ->first();

        $exists_end = Schedule::select(['time_end'])
            ->where('room_id', $room_id)
            ->where('date', $date)
            ->where('time_end', $time_end)
            ->where('status', 'approved')
            ->first();

        if ($exists_end) return false;

        $exists_end2 = Schedule::select(['time_start'])
            ->where('room_id', $room_id)
            ->where('date', $date)
            ->where('time_start', $time_end)
            ->where('status', 'approved')
            ->first();


        if (!($exists_start && $exists_end) && !($exists_start2 || $exists_end2)) {
            $existing_schedule = DB::table('schedules')->where('room_id', $room_id)
                ->where('date', $date)
                ->where('deleted_at', null)
                ->where('status', 'approved')->get();

            foreach ($existing_schedule as $key => $value) {
                $schedule_time_range = explode(',', substr($value->time_range, 2, -2));
                if (in_array($time_start, $schedule_time_range)) return false;
                if (in_array($time_end, $schedule_time_range)) return false;
            }
        }

        return true;
    }
}
