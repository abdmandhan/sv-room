<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'room_type_id',
        'name',
        'capacity',
        'building_id',
        'is_active'
    ];

    public function room_type()
    {
        return $this->belongsTo(RoomType::class);
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class);
    }

    public function building()
    {
        return $this->belongsTo(Building::class);
    }
}
